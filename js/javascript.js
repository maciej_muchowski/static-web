let baseInfo = {"name": "Maciek", "surname": "Muchowski", "email": "maciej.muchowski@gmail.com", "dateOfBirth": "25.02.95"};

let education = {"schoolType": "Polibuda", "graduationDate": "XD"};

let experience = {"employerName": "AC/DC", "employedFrom": 2014};

let cv = {"baseInformation": baseInfo, "education": [education], "experience": [experience]};



function populateName() {
    populate(cv.baseInformation.name, "name")
}

function populateSurname() {
    populate(cv.baseInformation.surname, "surname")
}

function populateEmail() {
    populate(cv.baseInformation.email, "email")
}

function populateDateOfBirth() {
    populate(cv.baseInformation.dateOfBirth, "dateOfBirth")
}

function populateEducation() {
    let educationTextTable = [];
    for(let index in cv.education){
        educationTextTable.push("Typ szkoły: " + cv.education[index].schoolType);
        educationTextTable.push("Rok ukończenia: " + cv.education[index].graduationDate);
    }
    populate(educationTextTable.join("\n"), "education");
}
function populateExperience() {
    let experienceTextTable = [];
    for(let index in cv.education){
        experienceTextTable.push("Nazwa firmy: " + cv.experience[index].employerName);
        experienceTextTable.push("Lata pracy: " + cv.experience[index].employedFrom + " -");
    }
    populate(experienceTextTable.join("\n"), "experience");
}



function populate(data, targetId) {
    $("#" + targetId).text(data);


    // let targetElement = document.getElementById(targetId);
    //
    // targetElement.innerText = data;
}

function populateCvData() {
    populateName();
    populateSurname();
    populateEmail();
    populateDateOfBirth();
    populateEducation();
    populateExperience();
}

populateCvData();